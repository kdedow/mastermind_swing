/**   
 * Peg class - the peg class that holds our peg objects
 * 
 * EE422C programming assignment #3  
 * Name: Dedow, Karl and Patel, Kunal
 * UT EID: kfd235 and kbp466
 * Lab Section: 16825 (Wednesday at 10:30)
 * EE422C - Assignment #3
 * @author Karl Dedow and Kunal Patel
 * @version 1.1 2014-10-14
 * */

package Assignment2;

public class Pegs
{
	// Enum for the peg class, as well as instance variables
	enum Peg{Black, White}
	int NUM_PEGS;
	int blackPegs;
	int whitePegs;
	
	/**
	 * Constructor for the pegs class. Sets the number of peg colors (i.e. 2), as well as
	 * initializes the black peg and white peg integer variables. The peg object represent 
	 * the amount of returned pegs per guess.
	 * 
	 */
	public Pegs()
	{
		NUM_PEGS = 2;
		blackPegs = 0;
		whitePegs = 0;
	}
	
	/**
	 * This method is called when a match for both color and location is found between the
	 * CPU code and the user guess. Adds one to the black pegs integer variable.
	 */
	public void addBlackPeg()
	{
		blackPegs += 1;
	}
	
	/**
	 * This method is called when a match for only the color is found between the
	 * CPU code and the user guess. Adds one to the white pegs integer variable.
	 */
	public void addWhitePeg()
	{
		whitePegs += 1;
	}
}
