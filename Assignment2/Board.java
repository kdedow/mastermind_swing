/**   
 * Player class for the mastermind experiment
 * Provides logic regarding player input (for guessed code) and guess checking 
 * EE422C programming assignment #3  
 * Name: Dedow, Karl and Patel, Kunal
 * UT EID: kfd235 and kbp466
 * Lab Section: 16825 (Wednesday at 10:30)
 * EE422C - Assignment #3
 * @author Karl Dedow and Kunal Patel
 * @version 1.1 2014-10-14
 * */

package Assignment2;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class Board
{
	// Instance variables associated with player
	// Number of allowed guesses and length of the code are both static
	static int NUM_GUESSES = 15;
	static int NUM_SPACES = 5;
	Boolean mode;
	Guess[] guesses;
	Pegs[] pegHistory;
	Colors[] tempCode;
	Colors[] tempPlayer;
	Scanner scan;
	Pegs pegs;

	/**
	 * Constructor for the player class. Implements a guess array to store each guess (as a Guess object), a
	 * history array to store each peg response, a temporary Colors array for the CPU code (used in the compare
	 * method), a temporary Colors array for the guessed code (used in the compare method), a scanner
	 * for all inputs from the console, a boolean for the type of game mode.
	 * 
	 * @param modeType a Boolean value representing the game mode (test vs. production)
	 */
	public Board(Boolean modeType)
	{
		guesses = new Guess[NUM_GUESSES];
		pegHistory = new Pegs[NUM_GUESSES];
		tempCode = new Colors[NUM_SPACES];
		tempPlayer = new Colors[NUM_SPACES];
		mode = modeType;
		scan = new Scanner(System.in);
	}
	
	/**
	 * Play a game of mastermind. This method starts the game by asking for player guesses.
	 * The method utilizes a for loop equal to the amount of guesses the user is allowed.
	 * If the user guesses the correct code (i.e. four black pegs) break out of the for
	 * loop since no more guesses are necessary
	 *       
	 */
	public void playGame()
	{
		// Create the CPU (in other words new code object) and a new player (i.e. player object)
		Code cpuCode = new Code(NUM_SPACES);
		cpuCode.generateCode();
		
		// For loop is the size of the amount of guesses allowed. In this implementation
		// 12 player guesses are allowed.
		for (int i = 0; i < NUM_GUESSES; i++)
		{
			pegs = new Pegs();
			tempCode = new Colors[NUM_SPACES];
			tempPlayer = new Colors[NUM_SPACES];
			// Make a guess, test the guess, create the response string, and check to see if the player
			// guess equals the user guess.
			if(makeGuess(i, cpuCode.code)){ break; }
			checkGuess(cpuCode.code, i);
			pegString(i);
			
			// Check the guess, if the user wins then break out of the for loop
			if (winCheck(cpuCode.code))
			{
				break;
				
			// If the last available guess is wrong, the player loses the game
			} else if (i == NUM_GUESSES - 1)
			{
				String tempMess = ("LOSER!\nCorrect code: " + Guess.toString(NUM_SPACES, cpuCode.code));
				JOptionPane.showMessageDialog(null, tempMess);
			}
		}
	}

	/**
	 * Allows the player to make a guess to solve the CPU code. The method asks
	 * for an input from the console that represents the four color code. If a
	 * wrong color or improperly sized code input is detected and exception is
	 * thrown that handles the situation.
	 * 
	 * @param numGuess the integer that represents the guess number, so as to store
	 *                 each guess in the correct spot in the guesses array
	 *
	 * @param code the array (consisting of colors in the Colors enum) that represents
	 *             the CPU code
	 *              
	 * @return endGame a Boolean value that determines whether the player wants to prematurely
	 *   						end the game (by clicking on cancel in the option pane)              
	 * 
	 */
	public Boolean makeGuess(int numGuess, Colors[] code)
	{
		// Get input that represents player guess
		Boolean endGame = false;
		StringBuilder message = new StringBuilder("You have " + (NUM_GUESSES - numGuess) + " guesses left.\n");
		// If we are in test mode, print the CPU code to the console
		if (mode)
		{
			message.append("CPU Code: ");
			message.append(Guess.toString(NUM_SPACES, code) + "\n");
		}
		message.append("What is your next guess?");
		String guess = JOptionPane.showInputDialog(null,message);
		// If null then quit the current game
		if (guess == null)
		{
			System.out.println("Ending Game...");
			endGame = true;
		// if the input asks for the history, call the history method then ask for another guess
		} else if(guess.equalsIgnoreCase("history"))
		{
			history(numGuess);
			endGame = makeGuess(numGuess, code);
		// Get a list of valid colors if the player inputs "colors". Then ask for another guess.
		} else if(guess.equalsIgnoreCase("colors"))
		{
			getColors();
			endGame = makeGuess(numGuess, code);
		} else
		{
			// Convert the recorded guess string to a Colors[] array
			Guess colorGuess = new Guess(guess);
			
			// store the inputted guess in the guesses array (so that we can have a history of guesses)
			guesses[numGuess] = colorGuess;
			try
			{
				// make sure the input is constructed correctly, throw an exception if not
				testInput(numGuess);
			} catch (MastermindException mess)
			{
				// Catch the exception then call makeGuess method again
				JOptionPane.showMessageDialog(null, "-> " + mess.getMessage());
				endGame = makeGuess(numGuess, code);
			}
		}
		return endGame;
	}

	/**
	 * Checks the player guess (code) with the CPU code. This method will
	 * eventually generate the correct number of black and white pegs (which
	 * are stored as instance variables) to return to the user for feedback.
	 * 
	 * @param cpuCode the Colors array that represents the computer generated code
	 *           
	 * @param numGuess the integer that represents the current guess 
	 */
	public void checkGuess(Colors[] cpuCode, int numGuess)
	{
		tempCode = cpuCode.clone();
		tempPlayer = guesses[numGuess].getGuess().clone();

		// Calls method that checks for black pegs.
		blackPeg();

		// Calls method that checks for white pegs
		whitePeg();
	}

	/**
	 * 
	 * Checks for black pegs. Black pegs are returned whenever there is an exact
	 * match between the player guess and the CPU code with regards to both color
	 * and location. To do this, each array index in the player and CPU code
	 * arrays are compared at their corresponding indexes. If there is a
	 * color match, the blackPegs variables (from the Peg object) is increased by one, and
	 * the matching colors in the temporary Colors array representing the player
	 * and CPU codes are replaced with different values. This guarantees
	 * matches are not duplicated (which would lead to an incorrect feedback to
	 * the player).
	 * 
	 */
	private void blackPeg()
	{
		// Compares each character in the temporary CPU and player strings
		for (int i = 0; i < NUM_SPACES; i++)
		{
			// Checks for character matches within each string
			if (tempCode[i] == tempPlayer[i])
			{
				pegs.addBlackPeg();
				// Replace with different values when a match is found
				tempCode[i] = Colors.REPLACE_CODE;
				tempPlayer[i] = Colors.REPLACE_GUESS;
			}
		}
	}

	/**
	 * 
	 * Checks for white pegs. White pegs are returned whenever there is a match
	 * in colors between the player guess and the CPU code. Also, they cannot be
	 * in matching locations. To do this, each color in the CPU code is
	 * compared to all the colors in the player code. Since the black pegs
	 * have already recorded and removed there will be no exact matches. If a
	 * match between colors (but not a match in location) is found, the whitePegs
	 * variable (from the Peg object) will be increased by one, and the relevant colors
	 * in the temporary arrays representing the player and CPU codes are replaced
	 * with different values. This guarantees matches are not duplicated
	 * (which would lead to an incorrect feedback to the player). Also, if a
	 * color match is found, the method will immediately increase the index for
	 * the CPU code and reset the index for the player code. This ensures that
	 * one matching color in the CPU code will not ever result in more than one
	 * white peg recorded (e.g. CPU: RYGG and Player: YRRG will not result in two
	 * white pegs when comparing R).
	 * 
	 */
	private void whitePeg()
	{
		// outer loop for the CPU code's index
		for (int i = 0; i < NUM_SPACES; i++)
		{
			// inner loop for the player's guess index
			for (int j = 0; j < NUM_SPACES; j++)
			{
				// Compare index's to see if there is a match, if so increase
				// whitePegs and break out of loop
				if (tempCode[i] == tempPlayer[j])
				{
					pegs.addWhitePeg();
					// Replace with different values when a match is found
					tempCode[i] = Colors.REPLACE_CODE;
					tempPlayer[j] = Colors.REPLACE_GUESS;
					break;
				}
			}
		}
	}

	/**
	 * Constructs the pegHistory array, which is the array that saves the
	 * feedback for each guess (i.e. the amount of black pegs and white pegs the
	 * user's guess returned). The method functions by storing the current Peg
	 * object in the correct index in the pegHistory array, then forming a string
	 * to output to the popup window. The string is a representation of the result
	 * of the guess (i.e. the amount of black pegs and white pegs returned)
	 * 
    * @param numGuess - the integer that represents the current guess 
	 */
	public void pegString(int numGuess)
	{
		pegHistory[numGuess] = pegs;
		// Construct the result string
		String tempPeg = pegToString(numGuess);
		// Output the correct amount of black and white pegs
		JOptionPane.showMessageDialog(null, tempPeg);
	}
	
	/**
	 * Converts the result (i.e. the amount of pegs the guess resulted in) to a string.
	 * The string will then be outputted to the popup, so as to give the user visual
	 * feedback of the result of their guess.
	 * 
	 * @param numGuess the integer that represents the current guess 
	 * 
	 * @return the String representation of the peg result
	 */
	public String pegToString(int numGuess)
	{
		// Initialize references to the user's guess and result
		Pegs pegs = pegHistory[numGuess];
		Colors[] guess = guesses[numGuess].getGuess();
		StringBuilder pegString = new StringBuilder(Guess.toString(NUM_SPACES, guess));
		
		// Build the string. If no pegs, customized output
		if(pegs.blackPegs == 0 && pegs.whitePegs == 0)
		{
			pegString.append(" -> Result: No pegs");
		}
		else
		{
			// Regular string output that represents the result of the guess
			pegString.append(" -> Result: " + pegs.blackPegs + " black peg(s), " + pegs.whitePegs + " white pegs(s)"); 
		}
		return pegString.toString();
	}

	/**
	 * Checks to see if user guess is equal to the CPU code. If so, the user has
	 * won the game and the the method will return true. If not, the user has not
	 * yet found the correct code and the method will return false
	 * 
	 * @param code the computer generated code (Colors[]). In other words, the correct code
	 *           
	 * @return the Boolean object that tells us if the user has won the game
	 *         (true means the user won, false means the guessed code is not
	 *         equal to the CPU code yet).
	 */
	public boolean winCheck(Colors[] code)
	{
		// If there are four black pegs then the user has all the colors in the
		// right spot
		if (pegs.blackPegs == NUM_SPACES)
		{
			// The user won!
			String output = new String("You Win!!!\n Computer generated code: " + Guess.toString(NUM_SPACES, code));
			JOptionPane.showMessageDialog(null, output);
			return true;
		} else
		{
			return false;
		}
	}

	/**
	 * Gives the history of guesses and their results. Since each guess stores
	 * the value of the guess (i.e. guesses) in an array, and the results of each
	 * guess in an array (i.e. pegHistory), an array is
	 * constructed that relies on the guesses and results array. This newly
	 * constructed multi-dimensional array is then printed to the console.
	 * 
	 * @param numGuess the integer that represents how many guesses the player has
	 *                 already inputted
	 */
	private void history(int numGuess)
	{
		// Create the array
		int arrySize = (numGuess + 1) * 3;
		Object[] table = new String[arrySize];
		table[0] = "Guess Number";
		table[1] = "      Guess";
		table [2] = "Result";
		
		// Fill in the multi-dimensional array
		for (int i = 1; i < arrySize / 3; i++)
		{
			String guessTemp = Guess.toString(NUM_SPACES, (guesses[i - 1].getGuess()));
			String pegTemp = pegToString(i -1);
			table[3*i] = String.valueOf(i);
			table[3*i + 1] = "      " + guessTemp;
			table[3*i + 2] = "                       " + pegTemp;
		}
		
		// Build the output string by using HTML. Loop through newly created array
		StringBuilder outArray = new StringBuilder();
		outArray.append("<html><h3 align='middle'>History of Guesses</h3><table border='0' cellpadding='10px'>");
		for(int i = 0; i < arrySize / 3; i++)
		{
			// Create an HTML table
			outArray.append("<tr><td align='left'>");
			outArray.append(table[3*i]);
			outArray.append("</td><td align='middle'>");
			outArray.append(table[3*i + 1]);
			outArray.append("</td>");
			outArray.append("</td><td>");
			outArray.append(table[3*i + 2]);
			outArray.append("</td></tr>");
		}
		JOptionPane.showMessageDialog(null, outArray.toString());
	}
	
	/**
	 * Builds a string that represents all available colors that the player has available (from which
	 * to formulate a guess).The string is built in response to a player requesting valid colors.
	 * The response is printed to a popup box.
	 */
	private void getColors()
	{
		StringBuilder tempColor = new StringBuilder("Available colors: ");
		
		// Loop through the array containing all the names of available colors
		for (int j = 0; j < Colors.NUM_COLORS; j++)
		{
			tempColor.append(Colors.COLOR_NAMES[j]);
			
			// Make sure there are no trailing commas
			if(j != Colors.NUM_COLORS - 1){ tempColor.append(", "); }
		}
		JOptionPane.showMessageDialog(null, tempColor);
	}

	/**
	 * Tests each user guess to make sure the guess only contains the right
	 * colors and is the right length. Does this by using the COLOR array that is
	 * in the color interface and making sure that only colors in the colors
	 * array are in the inputted player guess. Also, uses NUM_SPACES in the
	 * colors interface to make sure the inputted string is the correct length.
	 * Also, calls the repeate guesses function, so as to guarantee that there
	 * are no repeat guesses. If there is a repeat guess, throw an exception.
	 * 
	 * @param numGuess an integer that represents the current guess
	 * 
	 * @throws MastermindException if the guessed code is invalid, this exception is thrown
	 */
	private void testInput(int numGuess) throws MastermindException
	{
		// Toggle variable
		Colors[] userGuess = guesses[numGuess].getGuess();
		// Check to see if the guess is the correct length
		if (userGuess.length != NUM_SPACES)
		{
			throw new MastermindException("INVALID GUESS\n");
		}
		// check to see if the guess contains only the right colors
		else
		{
			// outer loop checks each character in the user guess
			for (int i = 0; i < NUM_SPACES; i++)
			{
				// inner loop checks each color in the COLORS array so as to
				// guarantee
				// each character in the guess is a valid color
				int j = 0;
				for(Colors color : Colors.values())
				{
					if (userGuess[i] == color)
					{
						// Matches a color, we are golden
						break;
					} else if (j == NUM_SPACES + 1)
					{
						// the character in the guess is not a valid color
						throw new MastermindException("INVALID GUESS\n");
					}
					j = j + 1;
				}
			}
		}
		
		// No exception so far, check for repeats
		if(checkRepeat(numGuess))
		{
		   // the guess is a repeat
			throw new MastermindException("This sequence is a repeat\n");
		}
	}
	
	/**
	 * Disallow any repeat guesses. Loops through the guesses array so as to 
	 * compare the current guess to previous guesses. If the current guess is
	 * equal to one of the previous guesses, the function returns true.
	 * 
	 * @param numGuess an integer that represents the current guess
	 * 
	 * @return a Boolean that indicates whether the current guess is a repeat or not. (true = repeat
	 * 		  false = not a repeat)
	 */
	private Boolean checkRepeat(int numGuess)
	{
		Colors[] currGuess = guesses[numGuess].getGuess();
		// Loops through all previous guesses
		for(int i = 0; i < numGuess; i++)
		{
			for(int j = 0; j < NUM_SPACES; j++)
			{
				// If the colors do not match, break inner loop and check next guess
				if(!(guesses[i].getGuess()[j] == currGuess[j])){ break; }
				// If all colors are a match then the current guess is a repeat.
				else if(j == NUM_SPACES - 1){ return true; }
			}
		}
		return false;
	}
}
