/**
 * An exception implementation that is related to the program.
 * All the exception does is allow a message to be printed to the console.
 * EE422C programming assignment #3  
 * Name: Dedow, Karl and Patel, Kunal
 * UT EID: kfd235 and kbp466
 * Lab Section: 16825 (Wednesday at 10:30)
 * EE422C - Assignment #3
 * @author Karl Dedow and Kunal Patel
 * @version 1.1 2014-10-14
 * */

package Assignment2;

public class MastermindException extends Exception
{
	/**
	 * Implement the exception. Store the specific message associated with the exception.
	 */
	private static final long serialVersionUID = 720414848058257732L;
	private String message;

	/**  
	 * Constructor for Mastermind Exception. Calls the superclass      
	 * 
	 */
	public MastermindException()
	{
		super();
	}

	/**
	 * Stores the message string associated with mastermind exception.
	 * 
	 * @param mess the message associated with the exception
	 *       
	 */
	public MastermindException(String mess)
	{
		super(mess);
		this.message = mess;
	}

	/**
	 * The cause of the exception. This simply calls the super class.
	 * 
	 * @param cause the throwable cause
	 */
	public MastermindException(Throwable cause)
	{
		super(cause);
	}

	/**
	 * Get the personalized message associated with the thrown exception.
	 * 
	 * @return this.message - the string message associated with the exception
	 *       
	 */
	@Override
	public String getMessage()
	{
		return this.message;
	}
}
