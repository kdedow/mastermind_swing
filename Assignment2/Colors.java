/**
 * The colors enum contains all the valid colors. Also, contains the amount of
 * the total amount of colors.
 *  
 * EE422C programming assignment #3  
 * Name: Dedow, Karl and Patel, Kunal
 * UT EID: kfd235 and kbp466
 * Lab Section: 16825 (Wednesday at 10:30)
 * EE422C - Assignment #3
 * @author Karl Dedow and Kunal Patel
 * @version 1.1 2014-10-14
 * */
package Assignment2;

public enum Colors
{
	// The color enum
	Blue('B'), 
	Red('R'), 
	Green('G'), 
	Orange('O'), 
	Purple('P'), 
	Yellow('Y'), 
	Maroon('M'),
	REPLACE_CODE('n'),
	REPLACE_GUESS('n');
	public static final int NUM_COLORS = 7;
	public static final String[] COLOR_NAMES = { "Blue", "Red", "Green", "Orange", "Purple", "Yellow", "Maroon" };
	
	// The color variable (to be associated with a Color)
   private char color;
   
	/**
	 * Constructs each color in the enum with an associated char value. The char is needed when
	 * constructing strings to output to the message boxes (e.g. for history)
	 * 
	 * @param color char that is associated with the colors
	 */
   Colors(char color) 
   {
      this.color = color;
   }

	/**
	 * Returns the character that is associated with the specified color in the enum.
	 * The char is needed when constructing strings to output to the message boxes (e.g. for history)
	 * 
	 * @return char that is associated with the color in the enum
	 */
   public char getColor() 
   {
      return color;
   }
}
