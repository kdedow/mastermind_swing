/**   
 * Guess class - this class creates an object representing a player guess
 * 
 * EE422C programming assignment #3  
 * Name: Dedow, Karl and Patel, Kunal
 * UT EID: kfd235 and kbp466
 * Lab Section: 16825 (Wednesday at 10:30)
 * EE422C - Assignment #3
 * @author Karl Dedow and Kunal Patel
 * @version 1.1 2014-10-14
 * */

package Assignment2;

public class Guess
{
	Colors[] guess;
	
	/**
	 * Constructs the Guess object. The Guess object is invoked whenever a new
	 * player guess is recorded by the popup box. Each guess object represents 
	 * a player's guess and is stored in the array of guesses in the board object,
	 * so as to effectively record a full history of guesses.
	 * 
	 * @param guessString a String representing a player guess
	 *           
	 */
	public Guess(String guessString)
	{
		// Populate the guess instance array
		guess = guessStringToObj(guessString);
	}

	/**
	 * Converts the user inputted guess (that is in String form) to a Colors[] array.
	 * That way, we can store each user guess as a collection of colors in the
	 * multi-dimensional guesses array without having to retain the original String format.
	 * 
	 * @param guess the String that represents the player's current guess
	 *           
	 * @return guessColors the Colors array that represents the converted user guess
	 */
	public Colors[] guessStringToObj(String guess)
	{
		Colors[] guessColors = new Colors[guess.length()];
		
		/* Loop through each character in the guess string, then associate the character
		 * with the associated color in the Colors enum. Lastly, store the color in the 
		 * correct index in the Colors[] array (i.e. guessColors)
	    */
		for(int j = 0 ; j < guess.length(); j++)
		{
			switch (guess.charAt(j))
			{
			case 'B':
				guessColors[j] = Colors.Blue;
				break;
			case 'R':
				guessColors[j] = Colors.Red;
				break;
			case 'G':
				guessColors[j] = Colors.Green;
				break;
			case 'O':
				guessColors[j] = Colors.Orange;
				break;
			case 'P':
				guessColors[j] = Colors.Purple;
				break;
			case 'Y':
				guessColors[j] =  Colors.Yellow;
				break;
			case 'M':
				guessColors[j] = Colors.Maroon;
				break;
			default:
				guessColors[j] = null;
			}
		}
		return guessColors;
	}
	
	/**
	 * Returns the intances array that represents the player's guess. The guess variable
	 * is an array of Colors enums and is a representation of a player guess
	 * 
	 * @return guess - the array representation of a player guess
	 */
	public Colors[] getGuess()
	{
		return this.guess;
	}
	
	/**
	 * Converts either a player guess or the CPU code into a string. Since both the player's guesses
	 * and the CPU code is stored as a Colors[] array (array of Color enums), this method is necessary
	 * to convert the array back into a string.
	 * 
	 * @param spaces an integer that represent the length of the guess/code input
	 * 
	 * @param code the Colors array that represent either a player guess or a user code
	 * 
	 * @return the String representation of either the player guess or the user code
	 */
	public static String toString(int spaces, Colors[] code)
	{
		StringBuilder pegString = new StringBuilder("");
		// Associates the a color Char with the Color enum
		for(int i = 0; i < spaces; i++)
		{
			// Appends the color char associated with the color
			pegString.append(code[i].getColor());
		}
		return pegString.toString();
	}
}
