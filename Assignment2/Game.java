/**   
 * Game class for the mastermind experiment
 * Provides logic regarding the creation of a new game 
 * EE422C programming assignment #3  
 * Name: Dedow, Karl and Patel, Kunal
 * UT EID: kfd235 and kbp466
 * Lab Section: 16825 (Wednesday at 10:30)
 * EE422C - Assignment #3
 * @author Karl Dedow and Kunal Patel
 * @version 1.1 2014-10-14
 * */

package Assignment2;

import java.util.Scanner;

public class Game
{
	Boolean gameMode;
	Scanner scan;

	/**
	 * Constructor for the colors class. Assigns a value for the game mode (true/false), and creates
	 * a new scanner for user inputs. Also provides a welcome method that describes the rules of the
	 * game.
	 * 
	 * @param mode Boolean value that represents the game mode (i.e. test or production)
	 */
	Game(Boolean mode)
	{
		gameMode = mode;
		scan = new Scanner(System.in);
		System.out.println("Welcome to Mastermind.  Here are the rules.\n");
		System.out
				.print("This is a text version of the classic board game Mastermind.\n"
						+ "The computer will think of a secret code. The code consists of 4 colored pegs.\n"
						+ "The pegs MUST be one of six colors: blue, green, orange, purple, red, or yellow. A color may\n"
						+ "appear more than once in the code. You try to guess what colored pegs are in the code and what\n"
						+ "order they are in.   After you make a valid guess the result (feedback) will be displayed.\n"
						+ "The result consists of a black peg for each peg you have guessed exactly correct (color and\n"
						+ "position) in your guess.  For each peg in the guess that is the correct color, but is out of position,\n"
						+ "you get a white peg.  For each peg, which is fully incorrect, you get no feedback.\n\n");
		System.out
				.print("Only the first letter of the color is displayed. B for Blue, R for Red, and so forth.\n"
						+ "When entering guesses you only need to enter the first character of each color as a capital letter.\n\n");
	}

	/**
	 * This method starts a new game, if the user indicates they would like to play. First
	 * the console asks the user if they want to play a game, and depending on the response
	 * the method will either return true (if the user indicates they want to play), or false
	 * (if the user indicates they do not want to play). An input of "Y" means the user wants to
	 * play and an input of "N" means the user doesn't not want to play. Program will end if user
	 * does not want to play
	 * 
	 * @param message the message to be displayed to console that will ask the user if they
	 *                want to play another game. Most likely this value will be "Would you 
	 *                like to play again?
	 *           
	 * @return status a Boolean object that indicates whether the player wants to play another 
	 * 		  game or not. True for another game and false for quit game
	 * 
	 */
	public Boolean nextGame(String message)
	{
		String bool = new String();
		Boolean status = new Boolean(null);
		System.out.println(message);
		System.out.print("(Y/N): ");
		String play = new String(scan.next());
		try
		{
			// Play another game if user input Y, return true.
			if (play.equalsIgnoreCase("y"))
			{
				bool = "y";
				status = true;
				
			// Quit game is user input N, return false.
			} else if (play.equalsIgnoreCase("n"))
			{
				bool = "n";
				status = false;
			}
			
			// Test that input is either "Y" or "N". Throw exception for any other value
			testInput(bool);
		} catch (MastermindException mess)
		{
			// Handle exception. Output message and call method again.
			System.out.println(mess.getMessage());
			status = nextGame("Enter the right input this time. Play game (Y/N)?");
		}
		return status;
	}

	/**
	 * Test the input when the player is asked if they want to play a new game.
	 * The input should either be "Y(y)" or "N(n)". Throw an exception if the 
	 * input is incorrect.
	 * 
	 * @param input the Boolean value that represents the game mode (i.e. test or production)
	 *        
	 * @throws MastermindException if the user input is invalid (that is not a Y or N value
	 * 									 throw an exception.
	 */
	public void testInput(String input) throws MastermindException
	{
		// Checks to see if input is correct
		if (input.equalsIgnoreCase("y") || input.equalsIgnoreCase("n"))
		{
			// no exception yay
		} else
		{
			// Input is incorrect throw an exception
			throw new MastermindException(
					"Invalid input. Please enter only Y or N");
		}
	}
}
