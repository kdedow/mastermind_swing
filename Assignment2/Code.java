/**   
 * Code class - this class generates the CPU code.
 * 
 * EE422C programming assignment #3  
 * Name: Dedow, Karl and Patel, Kunal
 * UT EID: kfd235 and kbp466
 * Lab Section: 16825 (Wednesday at 10:30)
 * EE422C - Assignment #3
 * @author Karl Dedow and Kunal Patel
 * @version 1.1 2014-10-14
 * */

package Assignment2;

import java.util.Random;

public class Code 
{
	int spaces;
	Colors[] code;
	Colors color;

	/**
	 * Constructor for the code class. This constructor sets the length of the 
	 * CPU code, as well as initializes the code array (which is of type Color)
	 * 
	 * @param numSpace the length of the CPU code
	 */
	public Code(int numSpace)
	{
		spaces = numSpace;
		code = new Colors[numSpace];
	}

	/**
	 * This method generates the actual CPU code. The method utilizes the Random object to 
	 * generate a number between 0 and NUM_COLORS - 1. This number is then associated with
	 * a specific color in the set of valid colors to get a character representing the
	 * color. From here, an array of Colors is built that will eventually form the CPU
	 * code for the current game
	 * 
	 */
	public void generateCode()
	{
		System.out.println("Generating secret code...\n");
		Random rand = new Random();

		// Generate a random number between 0 and NUM_COLORS - 1. Each number
		// will represent a specific color.
		for (int i = 0; i < spaces; i++)
		{
			// The number generator
			int num = rand.nextInt(Colors.NUM_COLORS);
			
			// Call buildCode(): buildCode() will associate a number with
			// a specific color char in the set of valid colors
			code[i] = buildCode(num);
		}
	}

	/**
	 * Associates the generated number with a color from our color array. Once the color in the 
	 * color enum is generated return the value, so that the CPU code strong can continue to be built.
	 * Essentially, this method converts an integer to a Color.
	 * 
	 * @param num a number that represents a specific color. Given the number, return a specific color
	 * 				 associated with that number.
	 * 
	 * @return the Color (from the Color enum) that the number is associated with
	 */
	private Colors buildCode(int num)
	{
		// The swtich statement associates a specific number with a specific color
		// Return the color
		switch (num)
		{
		case 0:
			return Colors.Blue;
		case 1:
			return Colors.Red;
		case 2:
			return Colors.Green;
		case 3:
			return Colors.Orange;
		case 4:
			return Colors.Purple;
		case 5:
			return Colors.Yellow;
		case 6:
			return Colors.Maroon;
		default:
			return null;
		}
	}
}
