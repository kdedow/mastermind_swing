/**   
 * Driver class for the project. Contains our main, that starts the program (or mastermind).
 * 
 * EE422C programming assignment #2  
 * Name: Dedow, Karl and Fernando, Rukshinie
 * UT EID: kfd235 and rf9447
 * Lab Section: 16825 (Wednesday at 10:30)
 * EE422C - Assignment #2
 * @author Karl Dedow and Rukshinie Fernando
 * @version 1.0 2014-10-02
 * */
package Assignment2;

import java.util.Scanner;

public class Driver
{
	public static void main(String args[])
	{
		Boolean bool = new Boolean(null);
		String gameMode = new String();
		String mode = new String();
		Scanner scan = new Scanner(System.in);
		
		// Ask the player if they want to play the game in test or production mode. Test mode
		// will display the CPU code on every guess. Production will hide the CPU code.
		do
		{
			System.out
					.print("Would you like to play the game in test mode? Enter true/false: ");
			mode = new String(scan.next());
			
			// Checks to see what mode we are in. Assign a value to bool to represent the environment.
			if (mode.equalsIgnoreCase("true"))
			{
				bool = true;
				gameMode = "Testing Environment";
			} else if (mode.equalsIgnoreCase("false"))
			{
				bool = false;
				gameMode = "Running finalized version of the game";
			} else
			{
				// Incorrect input (should be either true or false). Output an error. User will only
				// exit do-while loop once the correct input is achieved.
				System.err
						.print("Error: Unknown game mode (please make sure to enter only true/false)\n");
			}
		} while (!mode.equalsIgnoreCase("true") && !mode.equalsIgnoreCase("false"));
		
		System.out.println(gameMode);
		
		// Create a new game.
		Game game = new Game(bool);
		if (!game.nextGame("You have " + Board.NUM_GUESSES + " guesses to figure out the secret code or you lose the game.  Are you ready to play?"))
		{
			System.out.println("You don't even want to play once? You're no fun.\nGoodbye!");
			System.exit(-1);
		}
		
		// The the game until the player no longer wants to play anymore
		do
		{
			Board player = new Board(bool);
			player.playGame();
		} while (game.nextGame("Would you like to play again?"));
		System.out.println("Goodbye! Please play again soon.");
		scan.close();
	}
}
